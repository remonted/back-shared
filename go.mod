module gitlab.com/remonted/back-shared

go 1.16

require (
	github.com/prometheus/client_golang v1.12.1
	go.mongodb.org/mongo-driver v1.8.4
)
