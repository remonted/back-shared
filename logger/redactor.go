package logger

import (
  "strings"
)

// Redactor is an interface for types that may contain sensitive information
// (like passwords), which shouldn't be printed to the log. The idea was found
// in relog as part of the vitness project.
type Redactor interface {
  Redacted() interface{}
}

// Redact returns a string of * having the same length as s.
func Redact(s string) string {
  return strings.Repeat("*", len(s))
}
