package db

import (
	"encoding/json"
	"net/http"
	"regexp"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// ListRequestBson struct for list request
type ListRequestBson struct {
	Query  bson.M
	Search *primitive.Regex
	Sort   *bson.M
	Limit  *int64
	Skip   *int64
}

// ParseListRequest returns instance of ListRequestBson from parsed http.Request
func ParseListRequest(r *http.Request) *ListRequestBson {
	queryParams := r.URL.Query()

	queryStr := queryParams.Get("query")
	searchStr := queryParams.Get("globalSearch")
	sortStr := queryParams.Get("sort")
	limitStr := queryParams.Get("limit")
	skipStr := queryParams.Get("skip")

	var limit *int64
	var skip *int64

	if limitStr != "" {
		limitConverted, err := strconv.ParseInt(limitStr, 10, 0)
		if err != nil {
			limit = new(int64)
		} else {
			limit = &limitConverted
		}
	}

	if skipStr != "" {
		skipConverted, err := strconv.ParseInt(skipStr, 10, 0)
		if err != nil {
			skip = new(int64)
		} else {
			skip = &skipConverted
		}
	}

	return &ListRequestBson{
		Query:  queryBsonFromJSON(&queryStr),
		Search: searchBsonFromJSON(searchStr),
		Sort:   SortBsonFromJSON(&sortStr),
		Limit:  limit,
		Skip:   skip,
	}
}

// GetBsonQuery returns unordered representation of a BSON document for query from list request
func (l *ListRequestBson) GetBsonQuery() bson.M {
	return l.Query
}

// ApplyFindOptionsToAggregationPipeline applied find options to pipeline
func (l *ListRequestBson) ApplyFindOptionsToAggregationPipeline(pipeline []bson.M) []bson.M {
	opts := l.GetFindOptions()

	if opts.Projection != nil {
		pipeline = append(pipeline, bson.M{
			"$project": &opts.Projection,
		})
	}

	if opts.Sort != nil {
		pipeline = append(pipeline, bson.M{
			"$sort": &opts.Sort,
		})
	}

	if opts.Skip != nil {
		pipeline = append(pipeline, bson.M{
			"$skip": &opts.Skip,
		})
	}

	if opts.Limit != nil {
		pipeline = append(pipeline, bson.M{
			"$limit": &opts.Limit,
		})
	}

	return pipeline
}

// GetFindOptions returns find options from list request
func (l *ListRequestBson) GetFindOptions() *options.FindOptions {
	opts := new(options.FindOptions)

	if l.Sort != nil {
		opts.Sort = l.Sort
	}

	if l.Skip != nil {
		opts.Skip = l.Skip
	}

	if l.Limit != nil {
		opts.Limit = l.Limit
	}

	return opts
}

// SortBsonFromJSON returns BSON struct with sorting options
func SortBsonFromJSON(sortStr *string) *bson.M {
	d := regexp.MustCompile(`,`)
	arr := d.Split(*sortStr, -1)

	bsonSort := bson.M{}

	for _, value := range arr {
		key := value
		val := 1

		if len(value) < 1 {
			continue
		}

		if string(value[0]) == "-" {
			val = -1
			key = value[1:]
		} else if string(value[0]) == "+" {
			key = value[1:]
		}

		bsonSort[key] = val
	}

	return &bsonSort
}

func queryBsonFromJSON(queryStr *string) bson.M {
	queryMap := make(map[string]interface{})

	bsonQuery := bson.M{}

	if *queryStr != "" {
		err := json.Unmarshal([]byte(*queryStr), &queryMap)
		if err != nil {
			// Prevent to use invalid filters
			bsonQuery["false"] = false

			return bsonQuery
		}
	}

	for key, value := range queryMap {
		switch key {
		case "$search":
			s := regexp.MustCompile(`=`)
			search := s.Split(value.(string), -1)
			if len(search) < 2 {
				break
			}

			f := regexp.MustCompile(`,`)
			fields := f.Split(search[0], -1)

			or := bson.A{}

			for _, field := range fields {
				or = append(or, bson.D{{field, primitive.Regex{
					Pattern: search[1],
					Options: "i",
				}}})
			}

			bsonQuery["$or"] = or

			break
		default:
			bsonQuery[key] = value
		}
	}

	return bsonQuery
}

func searchBsonFromJSON(searchStr string) *primitive.Regex {
	if searchStr == "" {
		return nil
	}

	return &primitive.Regex{
		Pattern: searchStr,
		Options: "i",
	}
}
