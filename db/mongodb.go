package db

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mongoDb struct {
	Db
	config *config
	client *mongo.Client
}

// NewDb return Db instance for MongoDB
func NewDb(uri string, dbName string) Db {
	c, err := mongo.NewClient(options.Client().ApplyURI(uri))
	if err != nil {
		panic(err.Error())
	}

	config := config{
		URI:    uri,
		DbName: dbName,
	}

	// ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	err = c.Connect(context.TODO())

	return &mongoDb{config: &config, client: c}
}

// GetClient returns Db connection client
func (db *mongoDb) GetClient() *mongo.Client {
	return db.client
}

// GetDb returns Db handler
func (db *mongoDb) GetDb(opts ...*options.DatabaseOptions) *mongo.Database {
	return db.client.Database(db.config.DbName, opts...)
}
