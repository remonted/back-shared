package db

import (
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type config struct {
	URI    string
	DbName string
}

// Db interface for database service
type Db interface {
	// GetClient returns Db connection client
	GetClient() *mongo.Client
	// GetDb returns Db handler
	GetDb(opts ...*options.DatabaseOptions) *mongo.Database
}
