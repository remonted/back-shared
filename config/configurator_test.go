package config

import (
	"reflect"
	"testing"
)

func TestInitConfig(t *testing.T) {
	tests := []struct {
		name string
		want Configurator
	}{
		{"", new(Config)},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewConfig(); reflect.TypeOf(got).Name() == "Configurator" {
				t.Errorf("NewConfig() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConfig_GetValue(t *testing.T) {
	key := "TEST_KEY"
	val := "TEST_VAL"
	env := make(map[string]string)
	env["TEST_KEY"] = val
	type fields struct {
		env map[string]string
	}
	type args struct {
		key string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		{"Returns VALUE when KEY exists", fields{env: env}, args{key: key}, val},
		{"Returns nil when KEY not exists", fields{env: env}, args{key: "NOT_EXISTS"}, ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Config{
				env: tt.fields.env,
			}
			if got := c.GetValue(tt.args.key); got != tt.want {
				t.Errorf("Config.GetValue() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConfig_HasValue(t *testing.T) {
	key := "TEST_KEY"
	val := "TEST_VAL"
	env := make(map[string]string)
	env["TEST_KEY"] = val

	type fields struct {
		env map[string]string
	}
	type args struct {
		key string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{"Returns true when KEY exists", fields{env: env}, args{key: key}, true},
		{"Returns false when KEY not exists", fields{env: env}, args{key: "NOT_EXISTS"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Config{
				env: tt.fields.env,
			}
			if got := c.HasValue(tt.args.key); got != tt.want {
				t.Errorf("Config.HasValue() = %v, want %v", got, tt.want)
			}
		})
	}
}
