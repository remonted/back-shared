package config

import (
	"os"
	"strconv"
)

// Configurator interface for configuration service
type Configurator interface {
	GetValue(key string) string
	GetIntValue(key string) int64
	HasValue(key string) bool
}

// Config struct for configurator service
type Config struct {
	env map[string]string
}

// NewConfig return instance of Configurator
func NewConfig() Configurator {
	c := new(Config)

	env, _ := DotEnvRead()

	c.env = env

	return c
}

// GetValue get string value by key
func (c *Config) GetValue(key string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}

	if val, ok := c.env[key]; ok {
		return val
	}

	return ""
}

// GetIntValue get int64 value by key
func (c *Config) GetIntValue(key string) int64 {
	if val, ok := os.LookupEnv(key); ok {
		i, _ := strconv.ParseInt(val, 10, 0)

		return i
	}

	if val, ok := c.env[key]; ok {
		i, _ := strconv.ParseInt(val, 10, 0)

		return i
	}

	return 0
}

// HasValue check is config with key exists
func (c *Config) HasValue(key string) bool {
	if _, ok := os.LookupEnv(key); ok {
		return true
	}

	if _, ok := c.env[key]; ok {
		return true
	}

	return false
}
