package metrics

import (
	"net/http"
	"runtime"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	metricGoGoroutines = "go_goroutines"
	metricGoThreads    = "go_threads"
)

// ServiceWrapper struct for metrics service
type ServiceWrapper struct {
	Metrics *Metrics
}

// InitMetricsServiceWrapper returns instance of metrics ServiceWrapper
func InitMetricsServiceWrapper(metrics *Metrics) *ServiceWrapper {
	h := new(ServiceWrapper)
	h.Metrics = metrics

	return h
}

// MetricsHandler metrics handler
func (h *ServiceWrapper) MetricsHandler() http.Handler {
	(*h.Metrics).RegGauge(metricGoGoroutines, metricGoGoroutines, "Number of goroutines that currently exist.")
	(*h.Metrics).RegGauge(metricGoThreads, metricGoThreads, "Number of OS threads created.")

	(*h.Metrics).SetGauge(metricGoGoroutines, float64(runtime.NumGoroutine()))

	n, _ := runtime.ThreadCreateProfile(nil)
	(*h.Metrics).SetGauge(metricGoThreads, float64(n))

	return promhttp.HandlerFor((*h.Metrics).GetRegistry(), promhttp.HandlerOpts{})
}
