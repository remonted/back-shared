package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
)

// Metrics interface for metrics service
type Metrics interface {
	GetRegistry() *prometheus.Registry

	RegGauge(key string, name string, help string)
	SetGauge(key string, val float64)
	GetGauge(key string) *prometheus.Gauge

	IncCounterVec(key string, lvs ...string)
	RegCounterVec(key string, name string, help string, labelNames []string)
	GetCounterVec(key string) *prometheus.CounterVec
}

type prometheusMetrics struct {
	counterVecs map[string]*prometheus.CounterVec
	gauges      map[string]*prometheus.Gauge
	registerer  *prometheus.Registry
}

// NewMetrics returns instance of Metrics
func NewMetrics() Metrics {
	m := new(prometheusMetrics)

	m.registerer = prometheus.NewRegistry()

	return m
}

// GetRegistry returns metrics registry collectors
func (m *prometheusMetrics) GetRegistry() *prometheus.Registry {
	return m.registerer
}

// RegGauge register Gauge metric
func (m *prometheusMetrics) RegGauge(key string, name string, help string) {
	g := prometheus.NewGauge(prometheus.GaugeOpts{
		Name: name,
		Help: help,
	})

	if err := m.registerer.Register(g); err != nil {
		if are, ok := err.(prometheus.AlreadyRegisteredError); ok {
			g = are.ExistingCollector.(prometheus.Gauge)
		} else {
			panic(err)
		}
	}

	if m.gauges == nil {
		m.gauges = map[string]*prometheus.Gauge{}
	}

	m.gauges[key] = &g
}

// SetGauge update Gauge metric by key
func (m *prometheusMetrics) SetGauge(key string, val float64) {
	(*m.GetGauge(key)).Set(val)
}

// GetGauge returns Gauge metric by key
func (m *prometheusMetrics) GetGauge(key string) *prometheus.Gauge {
	return m.gauges[key]
}

// RegCounterVec register CounterVec metric
func (m *prometheusMetrics) RegCounterVec(key string, name string, help string, labelNames []string) {
	cv := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: name,
			Help: help,
		},
		labelNames,
	)

	if err := m.registerer.Register(cv); err != nil {
		if are, ok := err.(prometheus.AlreadyRegisteredError); ok {
			cv = are.ExistingCollector.(*prometheus.CounterVec)
		} else {
			panic(err)
		}
	}

	if m.counterVecs == nil {
		m.counterVecs = map[string]*prometheus.CounterVec{}
	}

	m.counterVecs[key] = cv
}

// IncCounterVec increment CounterVec metric by key
func (m *prometheusMetrics) IncCounterVec(key string, lvs ...string) {
	m.GetCounterVec(key).WithLabelValues(lvs...).Inc()
}

// GetCounterVec get CounterVec metric by key
func (m *prometheusMetrics) GetCounterVec(key string) *prometheus.CounterVec {
	return m.counterVecs[key]
}
